import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class Guichet21Test {

    private WebDriver driver;

    @Before
    public void setUp() {
        ChromeOptions chrome = new ChromeOptions();
        chrome.addArguments("--no-sandbox");
        chrome.setExperimentalOption("useAutomationExtension", false);
        chrome.addArguments("--headless");
        chrome.addArguments("start-maximized"); // open Browser in maximized mode
        chrome.addArguments("disable-infobars"); // disabling infobars
        chrome.addArguments("--disable-extensions"); // disabling extensions
        chrome.addArguments("--disable-gpu"); // applicable to windows os only
        chrome.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems

        //chrome.addArguments("--proxy-server=46.102.106.38:13228");
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
        driver = new ChromeDriver(chrome);
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void guichet21() {
        try {
            driver.get("http://www.val-de-marne.gouv.fr/Prendre-un-rendez-vous");
            driver.findElement(By.linkText("Accepter")).click();
        } catch (NoSuchElementException exception) {
            //
        }
        driver.get("http://www.val-de-marne.gouv.fr/Prendre-un-rendez-vous");
        driver.findElement(By.linkText("pour la naturalisation")).click();
        driver.findElement(By.id("condition")).click();
        driver.findElement(By.name("nextButton")).click();
        driver.findElement(By.cssSelector(".Bligne:nth-child(2) > label")).click();
        driver.findElement(By.name("nextButton")).click();
        assertThat(driver.findElement(By.id("FormBookingCreate")).getText(), is("Il n\'existe plus de plage horaire libre pour votre demande de rendez-vous. Veuillez recommencer ultérieurement."));
    }

    @Test
    public void guichet22() {
        try {
            driver.get("http://www.val-de-marne.gouv.fr/Prendre-un-rendez-vous");
            driver.findElement(By.linkText("Accepter")).click();
        } catch (NoSuchElementException exception) {
            //
        }
        driver.get("http://www.val-de-marne.gouv.fr/Prendre-un-rendez-vous");
        driver.findElement(By.linkText("pour la naturalisation")).click();
        driver.findElement(By.id("condition")).click();
        driver.findElement(By.name("nextButton")).click();
        driver.findElement(By.cssSelector(".Bligne:nth-child(3) > label")).click();
        driver.findElement(By.name("nextButton")).click();
        assertThat(driver.findElement(By.id("FormBookingCreate")).getText(), is("Il n\'existe plus de plage horaire libre pour votre demande de rendez-vous. Veuillez recommencer ultérieurement."));
    }


    @Test
    public void guichet24() {
        try {
            driver.get("http://www.val-de-marne.gouv.fr/Prendre-un-rendez-vous");
            driver.findElement(By.linkText("Accepter")).click();
        } catch (NoSuchElementException exception) {
            //
        }
        driver.get("http://www.val-de-marne.gouv.fr/Prendre-un-rendez-vous");
        driver.findElement(By.linkText("pour la naturalisation")).click();
        driver.findElement(By.id("condition")).click();
        driver.findElement(By.name("nextButton")).click();
        driver.findElement(By.cssSelector(".Bligne:nth-child(4) > label")).click();
        driver.findElement(By.name("nextButton")).click();
        assertThat(driver.findElement(By.id("FormBookingCreate")).getText(), is("Il n\'existe plus de plage horaire libre pour votre demande de rendez-vous. Veuillez recommencer ultérieurement."));
    }
}